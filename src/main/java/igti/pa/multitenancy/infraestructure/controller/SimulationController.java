
package igti.pa.multitenancy.infraestructure.controller;

import igti.pa.multitenancy.application.core.simulation.SimulationService;
import igti.pa.multitenancy.domain.model.Simulation;
import igti.pa.multitenancy.infraestructure.dto.SimulationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/simulation")
public class SimulationController {

    @Autowired
    private SimulationService simulationService;


    @GetMapping
    public ResponseEntity<List<Simulation>> getAll() {
        return ResponseEntity.ok(this.simulationService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Simulation> getById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(this.simulationService.findById(id));
    }

    @PostMapping("/calc")
    public ResponseEntity<SimulationDto> calcSimulation(@RequestParam String valueImovel,  @RequestParam String loanAmount) {
        String vImovel = valueImovel.replace(".", "").replace(",", "");
        String lAmount = loanAmount.replace(".", "").replace(",", "");
        SimulationDto simulation = new SimulationDto();
        simulation.setValueImovel(Double.parseDouble(vImovel));
        simulation.setLoanAmount(Double.parseDouble(lAmount));
        return ResponseEntity.ok(this.simulationService.calc(simulation));
    }

    @PostMapping
    public ResponseEntity<Simulation> post(@RequestBody Simulation simulation) {
       return ResponseEntity.ok(this.simulationService.save(simulation));
    }

    @PutMapping
    public ResponseEntity<Simulation> put(@RequestBody Simulation simulation) {
        return ResponseEntity.ok(this.simulationService.save(simulation));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id) {
        this.simulationService.delete(id);
        return ResponseEntity.ok().build();
    }

}
