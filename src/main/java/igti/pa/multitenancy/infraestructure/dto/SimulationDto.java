package igti.pa.multitenancy.infraestructure.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimulationDto {

    private Integer id;
    private Double rendaMensalMin;
    private Double valorPrestaco;
    private Double valueImovel; // valor do imovel
    private Double loanAmount; // valor empréstimo
    private Integer deadline;
    private String reason;
    private Double tax;

}
