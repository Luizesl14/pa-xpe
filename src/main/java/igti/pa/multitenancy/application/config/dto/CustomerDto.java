

package igti.pa.multitenancy.application.config.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;

public class CustomerDto {

    private final Integer id;
    private final String firstName;
    private final String lastName;
    private String fantasyName;
    private String color;

    private String domain;


    @JsonCreator
    public CustomerDto(@JsonProperty("id") Integer id,
                       @JsonProperty("firstName") String firstName,
                       @JsonProperty("lastName") String lastName,
                       @JsonProperty("fantasyName") String fantasyName,
                       @JsonProperty("color") String color,
                       @JsonProperty("domain") String domain) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fantasyName = fantasyName;
        this.color = color;
        this.domain = domain;
    }

    @JsonCreator
    public CustomerDto(@JsonProperty("id") Integer id,
                       @JsonProperty("firstName") String firstName,
                       @JsonProperty("lastName") String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }



    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("fantasyName")
    public String getFantasyName() {
        return fantasyName;
    }

    @JsonProperty("color")
    public String getColor() {
        return color;
    }
    @JsonProperty("domain")
    public String getDomain() {
        return domain;
    }
}